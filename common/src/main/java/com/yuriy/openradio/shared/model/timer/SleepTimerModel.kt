/*
 * Copyright 2021 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.openradio.shared.model.timer

import android.content.Context
import androidx.lifecycle.ViewModel
import com.yuriy.openradio.shared.model.storage.SleepTimerStorage
import com.yuriy.openradio.shared.service.OpenRadioService
import java.util.*

/**
 * [SleepTimerModel] is a class that is responsible for preparing and managing the data for an Activity or a Fragment.
 * It also handles the communication of the Activity / Fragment with the rest of the application
 * (e.g. calling the business logic classes).
 * A [ViewModel] is always created in association with a scope (a fragment or an activity) and will be retained as
 * long as the scope is alive. E.g. if it is an Activity, until it is finished.
 */
class SleepTimerModel: ViewModel() {

    /**
     * Current implementation of the calendar.
     */
    private val mCalendar = Calendar.getInstance()

    /**
     * Returns whether or not timer is enabled by the user.
     *
     * @param context Application context.
     *
     * @return True if enabled, false otherwise.
     */
    fun isEnabled(context: Context): Boolean {
        return SleepTimerStorage.loadEnabled(context)
    }

    /**
     * Sets whether or not timer is enabled.
     *
     * @param context Application context.
     * @param value True if enabled, false otherwise.
     */
    fun setEnabled(context: Context, value: Boolean) {
        SleepTimerStorage.saveEnabled(context, value)
    }

    /**
     * Updates calendar with a timestamp either set by user or the system current.
     *
     * @param context Application context.
     * @param enabled Whether or not timer is enabled by the user.
     */
    fun updateTime(context: Context, enabled: Boolean) {
        if (enabled) {
            mCalendar.time = SleepTimerStorage.loadDate(context)
        } else {
            mCalendar.time = Date(System.currentTimeMillis())
        }
    }

    /**
     * Updates timer logic itself. Communicates with the service and provides value of the timestamp to be triggered
     * at if enabled, or disable the service otherwise.
     */
    fun updateTimer(context: Context, time: Long, enabled: Boolean) {
        SleepTimerStorage.saveDate(context, time)
        context.startService(OpenRadioService.makeSleepTimerIntent(context, enabled, time))
    }

    /**
     * Sets the date of the calendar.
     *
     * @param year Year to set.
     * @param month Month to set. The first month of the year in the Gregorian and Julian calendars is JANUARY
     *              which is 0; the last depends on the number of months in a year.
     * @param day Day of the month. This is a synonym for DATE. The first day of the month has value 1.
     */
    fun setDate(year: Int, month: Int, day: Int) {
        mCalendar.set(Calendar.YEAR, year)
        mCalendar.set(Calendar.MONTH, month)
        mCalendar.set(Calendar.DAY_OF_MONTH, day)
    }

    /**
     * Sets time of the calendar.
     *
     * @param hourOfDay Hour of the day. It is used for the 24-hour clock.
     *                  E.g., at 10:04:15.250 PM the HOUR_OF_DAY is 22.
     * @param minute Minute within the hour. E.g., at 10:04:15.250 PM the MINUTE is 4.
     */
    fun setTime(hourOfDay: Int, minute: Int) {
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        mCalendar.set(Calendar.MINUTE, minute)
        mCalendar.set(Calendar.SECOND, 0)
        mCalendar.set(Calendar.MILLISECOND, 0)
    }

    /**
     * Returns a [Date] object representing calendar's time value.
     *
     * @return Time representation.
     */
    fun getTime(): Date {
        return mCalendar.time
    }

    /**
     * returns current timestamp of the calendar based on the current state of the calendar.
     *
     * @return Returns the number of milliseconds since January 1, 1970, 00:00:00 GMT represented by this Date object.
     */
    fun getTimestamp(): Long {
        return mCalendar.time.time
    }

    /**
     * Helper function to validate whether or not timestamp is not valid for processing.
     *
     * @return True if not valid, false otherwise.
     */
    fun isTimestampNotValid(value: Long): Boolean {
        return value <= System.currentTimeMillis()
    }
}
